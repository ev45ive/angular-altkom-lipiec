# Git
cd ..
git clone https://bitbucket.org/ev45ive/angular-altkom-lipiec.git
cd angular-altkom-lipiec
npm i 
ng s

# Node

https://nodejs.org/en/download/

node -v
npm -v

git --version
https://git-scm.com/download/win

code -v

# Angular language Service
https://marketplace.visualstudio.com/items?itemName=Angular.ng-template

# VS CODE
https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2
https://marketplace.visualstudio.com/items?itemName=stringham.move-ts
https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig

https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype
https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

# CLI
npm install -g @angular/cli
C:\Users\<User>\AppData\Roaming\npm\ng -> C:\Users\ev45i\AppData\Roaming\npm\node_modules\@angular\cli\bin\ng

ng version
ng help

# Update
https://update.angular.io/

# Powershell
ng.cmd new angular-open-lipiec

cd ..
ng new angular-open-lipiec
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS   [ https://sass-lang.com/documentation/syntax#scss                ]


# Git 
https://git-scm.com/download/win
git init 
git add .
git config --global user.name "Mateusz"
git config --global user.email "Mateusz"
git commit -m "Initial commit"

# Serve local
cd angular-open-lipiec
ng serve
ng s -o 

# Generators
ng g m playlists -m app --routing
ng g c playlists/views/playlists
ng g c playlists/components/playlist-list
ng g c playlists/components/playlist-list-item
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-form

# Augury
https://augury.rangle.io/


# Shared + Pipe
ng g m shared -m playlists
ng g p shared/yesno --export 

# Core module
ng g m core -m app
ng g interface core/model/playlist

# Modules
- AppModule
- RoutingModule
- CoreModule
- FeatureModules
  - RoutingChildModule
- SharedModule

# LIFT
- Localisation
- Identification
- Flat
- Try TO be DRY (dont-repeat-yourself)

# Music Search
ng g m music-search -m app --routing --route music
ng g c music-search/views/album-search
ng g c music-search/views/album-details 

ng g c music-search/components/search-form
ng g c music-search/components/search-results
ng g c music-search/components/album-card

ng g i core/model/search

ng g s music-search/services/album-search

# Autorization
ng g m core/security -m core
ng g s core/security/auth

ng g interceptor core/security/auth

# USer profile
ng g s core/user --flat false