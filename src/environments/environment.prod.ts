
import { AuthConfig } from 'src/app/core/security/auth.service'

export const environment = {
  production: true,
  music: {
    api_url: 'https://api.spotify.com/v1/search'
  },
  auth: {
    auth_url: 'https://accounts.spotify.com/authorize',
    client_id: '4074b0663bad404992b2087ebdd67790',
    redirect_uri: 'http://localhost:4200/',
    response_type: 'token',
    scopes: [
      'playlist-read-collaborative',
      'playlist-modify-public',
      'playlist-read-private',
      'playlist-modify-private',
    ],
    show_dialog: true,
  } as AuthConfig
};
