import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MusicSearchComponent } from './music-search.component';
import { AlbumDetailsComponent } from './views/album-details/album-details.component';
import { AlbumSearchComponent } from './views/album-search/album-search.component';
import { SyncSearchComponent } from './views/sync-search/sync-search.component';

/* /music, chidlren: */
const routes: Routes = [
  {
    path: '',
    component: MusicSearchComponent,
    children: [
      {
        path: '',
        redirectTo: 'search', pathMatch: 'full'
      },
      {
        path: 'search',
        component: AlbumSearchComponent
      },
      {
        path: 'album',
        component: AlbumDetailsComponent
      },
      {
        path: 'sync',
        component: SyncSearchComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicSearchRoutingModule { }
