import { Injectable, Inject } from '@angular/core';
import { mockData } from '../views/album-search/mockData';
import { Album } from 'src/app/core/model/Album';

import { SEARCH_API_URL } from './tokens'
import { MusicSearchModule } from '../music-search.module';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from 'src/app/core/security/auth.service';
import { AlbumSearchResults } from 'src/app/core/model/search';
import { of, from, EMPTY, NEVER, throwError, BehaviorSubject, ReplaySubject } from 'rxjs';
import { map, pluck, catchError, mergeAll, switchAll, exhaust, switchMap, startWith } from 'rxjs/operators';

@Injectable({
  providedIn: 'any'
})
export class AlbumSearchService {
  private errors = new ReplaySubject<Error>(3, 5_000);

  private query = new BehaviorSubject<string>('');
  queryChange = this.query.asObservable().pipe(
    // startWith(1),
    // tap(()=>debugger)
  )

  private results = new BehaviorSubject<Album[]>([]);
  resultsChange = this.results.asObservable()


  constructor(
    @Inject(SEARCH_API_URL) private api_url: string,
    private http: HttpClient) {

    (window as any).subject = this.results


    this.queryChange.pipe(
      map(query => ({
        type: 'album', query: query
      })),
      switchMap(params => this.fetchResults(params))
    ).subscribe({
      next: albums => this.results.next(albums)
    });
  }

  fetchResults(params: { type: string; query: string; }) {
    return this.http.get<AlbumSearchResults>(this.api_url, { params }).pipe(
      map(resp => resp.albums.items),
      catchError((error: Error) => {
        this.errors.next(error)
        return EMPTY;
      })
    )
  }

  search(query: string) {
    this.query.next(query)
  }
}
