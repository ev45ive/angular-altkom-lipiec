import { InjectionToken } from '@angular/core';
const tokens = {};
export const SEARCH_API_URL = new InjectionToken('Search url api for albums');
