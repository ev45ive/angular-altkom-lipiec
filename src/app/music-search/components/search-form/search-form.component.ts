import { Component, OnInit, EventEmitter, Output, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { map, distinctUntilChanged, filter, debounceTime, mergeAll, throttleTime } from 'rxjs/operators';
import { merge, scheduled, from } from 'rxjs';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit/* , OnChanges */ {

  @Input()
  set query(q: string) {
    (this.searchForm.get('query') as FormControl)?.setValue(q, {
      emitEvent: false,
      onlySelf: true
    })
  }

  @Output() search = new EventEmitter<string>();

  // ngOnChanges(changes: SimpleChanges): void {
  //   this.searchForm.get('query')?.setValue(changes['query'].currentValue)
  // }  

  searchForm = new FormGroup({
    query: new FormControl('batman')
  })

  private searchClick = new EventEmitter();

  constructor() {
    (window as any).form = (this.searchForm)

    const valueChanges = this.searchForm.valueChanges

    const queryChanges = valueChanges.pipe<string>(
      map(value => value.query.trim())
    )
    const searchClick = this.searchClick.asObservable()


    from([
      queryChanges.pipe(
        distinctUntilChanged(),
        debounceTime(400)),

      searchClick.pipe(
        map(() => this.searchForm.value.query),
        throttleTime(1000)
      )

    ]).pipe(
      mergeAll(),
      filter(q => q.length >= 3),
    )
      // .subscribe(this.search)
      .subscribe(query => this.search.emit(query))

  }

  ngOnInit(): void {
  }

  emitSearch() {
    this.searchClick.emit()
  }

}
