import { Album } from 'src/app/core/model/Album';

export const mockData: Partial<Album>[] = [
  {
    name: 'Test 123',
    images: [
      {
        height: 300,
        width: 300,
        url: 'https://www.placecage.com/c/300/300'
      }
    ]
  },
  {
    name: 'Test 234',
    images: [
      {
        height: 300,
        width: 300,
        url: 'https://www.placecage.com/c/300/300'
      }
    ]
  },
  {
    name: 'Test 345',
    images: [
      {
        height: 300,
        width: 300,
        url: 'https://www.placecage.com/c/300/300'
      }
    ]
  },
];
