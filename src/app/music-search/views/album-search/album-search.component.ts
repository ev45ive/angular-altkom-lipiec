import { Component, OnInit, Inject } from '@angular/core';
import { AlbumSearchService } from '../../services/album-search.service'

@Component({
  selector: 'app-album-search',
  templateUrl: './album-search.component.html',
  styleUrls: ['./album-search.component.scss']
})
export class AlbumSearchComponent implements OnInit {

  query = this.service.queryChange
  results = this.service.resultsChange
  // results = this.service.fetchResults({ type: 'album', query: 'batman' })

  message = ''

  constructor(private service: AlbumSearchService) { }

  ngOnInit(): void {   }

  refreshResults(){
    // AsyncPipe will resubscribe to new object
    this.results = this.service.fetchResults({ type: 'album', query: 'superman' })
  }

  search(query = 'batman') {
    this.service.search(query)
  }

}

