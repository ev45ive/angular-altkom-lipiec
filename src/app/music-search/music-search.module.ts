import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicSearchRoutingModule } from './music-search-routing.module';
import { MusicSearchComponent } from './music-search.component';
import { AlbumDetailsComponent } from './views/album-details/album-details.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { AlbumSearchComponent } from './views/album-search/album-search.component';
import { environment } from 'src/environments/environment';

import { SEARCH_API_URL } from './services/tokens'
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SyncSearchComponent } from './views/sync-search/sync-search.component';

@NgModule({
  declarations: [
    MusicSearchComponent, AlbumDetailsComponent, SearchFormComponent, SearchResultsComponent, AlbumCardComponent, AlbumSearchComponent, SyncSearchComponent
  ],
  imports: [
    CommonModule,
    MusicSearchRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
    {
      provide: SEARCH_API_URL,
      useValue: environment.music.api_url
    },
    // {
    //   provide: AlbumSearchService,
    //   useFactory(api_url: string) {
    //     return new AlbumSearchService(api_url)
    //   },
    //   deps: [SEARCH_API_URL/* , XXX */]
    // },
    // {
    //   provide: AbstractSearchService,
    //   useClass: SpotifyAlbumSearchService,
    //   // deps: [SEARCH_API_URL]
    // },
    // AlbumSearchService
  ]
})
export class MusicSearchModule { }
