import { BrowserModule } from '@angular/platform-browser';
import { NgModule, DoBootstrap, ApplicationRef } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { CoreModule } from './core/core.module';
import { SEARCH_API_URL } from './music-search/services/tokens';
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    PlaylistsModule,
    AppRoutingModule
  ],
  providers: [
  
  ],
  bootstrap: [AppComponent]
})
export class AppModule /* implements DoBootstrap */ {

  constructor(/* auth */){}

  // ngDoBootstrap(appRef: ApplicationRef): void {
  //   appRef.bootstrap(AppComponent,'app-root')
  // }
}
