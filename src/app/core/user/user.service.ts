import { Injectable } from '@angular/core';
import { AuthService } from '../security/auth.service';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map, mapTo, filter, mergeAll, exhaust, distinctUntilChanged } from 'rxjs/operators';


export interface UserProfile {
  display_name: string;
  external_urls: {};
  followers: {};
  href: string;
  id: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private user = new BehaviorSubject<UserProfile | null>(null)

  userProfile = this.user.asObservable()

  constructor(public auth: AuthService, public http: HttpClient) { }

  init() {
    this.auth.isLoggedIn.pipe(filter(s => s == false))
    .subscribe(() => this.user.next(null))


    this.auth.isLoggedIn.pipe(
      distinctUntilChanged(),
      filter(s => s === true),
      map(state => this.http.get<UserProfile>('https://api.spotify.com/v1/me')),
      exhaust(), // one active at the time
    )
    .subscribe(user => {
      this.user.next(user)
    })

  }
}


