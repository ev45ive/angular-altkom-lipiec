import { Album } from "./Album";

export { Track } from "./Track";

export interface ArtistSearchResults {
  artists: PagingObject<Artist>;
}
export interface AlbumSearchResults {
  albums: PagingObject<Album>;
}

export interface PagingObject<Item> {
  href:     string;
  items:    Item[];
  limit:    number;
  next:     null;
  offset:   number;
  previous: null;
  total:    number;
}

export interface Artist {
  external_urls: ExternalUrls;
  genres:        any[];
  href:          string;
  id:            string;
  images:        Image[];
  name:          string;
  popularity:    number;
  type:          string;
  uri:           string;
}

export interface ExternalUrls {
  spotify: string;
}

export interface Copyright {
  text: string;
  type: string;
}

export interface ExternalIDS {
  upc: string;
}

export interface Image {
  height: number;
  url:    string;
  width:  number;
}


