export interface Entity {
  id: number | string;
  name: string;
}

enum EntityTypes {
  playlist = 'playlist', track = 'track'
}

export interface Playlist extends Entity {
  /**
   * Is public?
   */
  public: boolean;
  description: string;
  // tracks: Array<Track>
  // tracks: Track[] | undefined
  tracks?: Track[],
  // type: 'playlist'
}

export interface Track extends Entity {
  duration: number
  // type: 'track'
}

// interface Point { x: number; y: number }
// interface Vector { x: number; y: number, length:number }

// var p: Point = { x: 123, y: 123 }
// var v: Vector = { x: 123, y: 123, length:123 }
// v = p
// p = v 

// /api/v1/playlists > <Playlist>
// /api/v1/tracks > <Tracks>
// /api/v1/search?type=album > <Album>

// type Mix = Playlist | Track;

// const pl: Mix = {} as any

// if (pl!.type === 'playlist') {
//   pl.public
// } else {
//   pl.duration
// }

// Playlist.fromJSON({})

// if('string' === typeof pl.id){
//   pl.id.bold()
// }else{
//   pl.id.toExponential()
// }


// if(pl.tracks){
//   pl.tracks
// }else{
//   pl.tracks
// }