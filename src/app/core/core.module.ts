import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientModule } from '@angular/common/http';
import { SecurityModule } from './security/security.module'
import { environment } from 'src/environments/environment';
import { UserService } from './user/user.service'

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    SecurityModule.forRoot(environment.auth)
  ],
  providers:[
    // {
    //   provide: HttpClient,
    //   useClass: MySpecialHttpClient
    // }
  ]
})
export class CoreModule { 

  constructor(private user:UserService){
    user.init()
  }
}
