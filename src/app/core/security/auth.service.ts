import { Injectable, EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpParams } from '@angular/common/http';
import { Subject, ReplaySubject, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'any',
  useValue: environment.auth
})
export abstract class AuthConfig {
  auth_url!: string
  client_id!: string
  response_type!: 'token'
  redirect_uri!: string
  state?: string
  scopes?: string[]
  show_dialog?: boolean
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  token: string | null = null

  // private state = new EventEmitter()
  // private state = new Subject<boolean>()
  // private state = new ReplaySubject<boolean>(3,10_000)
  // private state = new ReplaySubject<boolean>(1)
  private state = new BehaviorSubject<boolean>(false)

  isLoggedIn = this.state.asObservable() 

  constructor(private config: AuthConfig) { 
    // this.state.getValue()
  }

  init() {
    this.extractToken()

    console.log('init')

    if (!this.token) {
      this.authorize()
    } else {
      this.state.next(true)
    }
  }

  logout() {
    sessionStorage.removeItem('token')
    this.token = null
    this.state.next(false)
  }

  extractToken() {
    const params = new HttpParams({
      fromString: window.location.hash
    })
    const token = params.get('#access_token')

    if (token) {
      window.location.hash = '';
      sessionStorage.setItem('token', JSON.stringify(token))
    }

    this.token = JSON.parse(sessionStorage.getItem('token') || 'null')
  }

  authorize() {
    sessionStorage.removeItem('token')

    const { auth_url, state, show_dialog, scopes, response_type, redirect_uri, client_id } = this.config

    const params = new HttpParams({
      fromObject: {
        response_type,
        redirect_uri,
        client_id,
        scope: scopes?.join(' ') || '',
        show_dialog: show_dialog && 'true' || '',
        state: state || ''
      }
    })
    window.location.href = (`${auth_url}?${params.toString()}`)
  }

  getToken() {
    return this.token
  }
}
