import { NgModule, ModuleWithProviders, Injectable, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService, AuthConfig } from './auth.service';
import { HTTP_INTERCEPTORS, HttpInterceptor, HttpClientXsrfModule, HttpXsrfTokenExtractor } from '@angular/common/http';

import { AuthInterceptor } from './auth.interceptor'

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    // HttpClientXsrfModule.withOptions({
    //   cookieName:'token',
    //   headerName:'Authorization'
    // })
    HttpClientXsrfModule.disable()
  ],
  providers: [
    // {
    //   provide: HttpXsrfTokenExtractor,
    //   useClass: AuthService
    // },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    // {
    //   useValue: environment.auth,
    //   provide: AuthConfig
    // }
  ]
})
export class SecurityModule {
  constructor(private auth: AuthService,
    @Inject(HTTP_INTERCEPTORS) intercetors: HttpInterceptor[]) {
    this.auth.init()
  }

  static forRoot(config: AuthConfig): ModuleWithProviders<SecurityModule> {
    return {
      ngModule: SecurityModule,
      providers: [
        {
          useValue: config,
          provide: AuthConfig
        }
      ]
    }
  }
}
