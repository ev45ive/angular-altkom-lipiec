import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  // template: `
  //   <div></div>
  // `,
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist!:Playlist 

  constructor() { }

  ngOnInit(): void {
  }

  @Output() edit = new EventEmitter();

  clickEdit(){
    this.edit.emit()
  }
}
