import { Component, OnInit, ChangeDetectorRef, Input, Output, EventEmitter, OnChanges, SimpleChanges, DoCheck, OnDestroy, ViewChild, AfterViewChecked, AfterViewInit } from '@angular/core';
import { Playlist } from '../../../core/model/playlist'
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-playlist-form',
  templateUrl: './playlist-form.component.html',
  styleUrls: ['./playlist-form.component.scss']
})
export class PlaylistFormComponent implements OnInit, AfterViewInit {

  @Input() playlist!: Playlist
  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter();


  @ViewChild('formRef', { read: NgForm })
  formRef?: NgForm

  constructor() { }

  ngAfterViewInit(): void {
    setTimeout(() => {
      console.log(this.formRef?.value)
      
      this.formRef?.valueChanges?.subscribe(value => {
        console.log(value)
      })
      
    })
  }

  ngOnInit(): void {
  }

  clickCancel() {
    this.cancel.emit()
  }

  clickSave(form: NgForm) {
    if (form.invalid) { return }

    const draft = {
      ...this.playlist,
      ...form.value
    }
    // console.log(form.value)
    console.log(draft)

    this.save.emit(draft)
  }

}
