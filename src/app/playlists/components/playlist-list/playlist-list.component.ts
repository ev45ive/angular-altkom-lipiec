import { Component, OnInit, ViewEncapsulation, Input, EventEmitter, Output } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';
import { NgForOf, NgForOfContext } from '@angular/common';


NgForOf
NgForOfContext

@Component({
  selector: 'app-playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.scss'],
  // encapsulation: ViewEncapsulation.Emulated,
  // inputs:['playlists:items']
})
export class PlaylistListComponent implements OnInit {

  @Input('items') 
  playlists: Playlist[] = []


  @Input()
  selected?: Playlist

  @Output() // zmiennaChange
  selectedChange = new EventEmitter<Playlist>()


  select(p: Playlist) {
    this.selectedChange.emit(p)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
