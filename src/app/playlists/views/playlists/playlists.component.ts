import { Component, OnInit } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist'

type ModesEnum = 'show' | 'edit';

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss']
})
export class PlaylistsComponent implements OnInit {

  mode: ModesEnum = 'show'

  playlists: Playlist[] = [
    {
      id: '123',
      name: 'Angular HITS',
      description: '',
      public: false,
    },
    {
      id: '234',
      name: 'Best of Angular',
      description: 'My favoruite',
      public: true,
    },
    {
      id: '345',
      name: 'Angular Top20',
      description: '',
      public: false,
    }
  ]

  selected?: Playlist


  constructor() { }

  ngOnInit(): void {
  }

  edit() {
    this.mode = 'edit'
  }

  cancel() {
    this.mode = 'show'
  }

  save(draft: Playlist) {
    this.mode = 'show'
    this.selected = draft;
    const index = this.playlists.findIndex(p => p.id == draft.id);
    if (index !== -1) {
      this.playlists[index] = draft
    }
  }

}
