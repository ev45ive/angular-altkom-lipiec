import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlaylistsComponent } from './views/playlists/playlists.component';


const routes: Routes = [
  {
    path: 'playlists',
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch:'full'
      },
      {
        path: 'list',
        component: PlaylistsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule { }
