import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { UserService, UserProfile } from './core/user/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'Angular App';
  profile: null | UserProfile = null

  constructor(private user: UserService) {
    this.user.userProfile.subscribe(user => {
      this.profile = user
    })
  }

}
