import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'playlists/list',
    pathMatch: 'full'
  },
  {
    path: 'music',
    loadChildren: () => import('./music-search/music-search.module')//
      .then(m => m.MusicSearchModule),
    data: {
      placki: true,
      secure: true
    },
  },
  {
    path: '**',
    redirectTo: 'music',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // enableTracing: true,
    // useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
